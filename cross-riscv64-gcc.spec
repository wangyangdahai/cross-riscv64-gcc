%global debug_package %{nil}
%global __os_install_post %{nil}

%define target_name riscv64-unknown-linux-gnu

Name:    %{target_name}
Version: 2.6.2
Release: 1
#Source0: https://occ-oss-prod.oss-cn-hangzhou.aliyuncs.com/resource//1695643844189/Xuantie-900-gcc-linux-5.10.4-glibc-x86_64-V2.6.2-20230916.tar.gz
Source0: xaa
Source1: xab
Summary: FIXME
License: FIXME

%description
Xuantie-900-gcc-linux-5.10.4-glibc-x86_64-V2.6.2-20230916.tar.gz

%prep

%build

%install

mkdir -p %{buildroot}/opt/riscv64/%{target_name}
cat %{SOURCE0} %{SOURCE1} > Xuantie-900-gcc-linux-5.10.4-glibc-x86_64-V2.6.2-20230916.tar.gz
tar xf Xuantie-900-gcc-linux-5.10.4-glibc-x86_64-V2.6.2-20230916.tar.gz --strip-components=1 -C %{buildroot}/opt/riscv64/%{target_name}

%files
/opt/riscv64/%{target_name}/*

%changelog
